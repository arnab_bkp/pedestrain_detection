
#include <stdio.h>
#include <iostream>
#include <limits>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "nms.hpp"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string.h>
#include <ctype.h>
#include <fstream>
#include <math.h>
#include<opencv2/core/core.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/objdetect/objdetect.hpp>
#include "nms.hpp"


using namespace std;
using namespace cv;




int main()   {

    // create a random test image
    Mat random(Size(200,200), DataType<float>::type);
    randn(random, 1, 1);

    // only look for local maxima above the value of 1
    Mat mask = (random > 1);

    // find the local maxima with a window of 50
    Mat maxima;
    nonMaximaSuppression(random, 10, maxima, mask);

    imshow ("source" , random);
    waitKey(0);

    imshow (" dest" , maxima);
    waitKey(0);

    // optionally set all non-maxima to zero
    random.setTo(0, maxima == 0);

}
