#include <iostream>
#include <opencv2/opencv.hpp>
#include <string.h>
#include <ctype.h>
#include <fstream>
#include <time.h>
#include <math.h>
#include<opencv2/core/core.hpp>
#include<opencv2/imgproc/imgproc.hpp>
#include<opencv2/highgui/highgui.hpp>
#include<opencv2/objdetect/objdetect.hpp>
//#include<ros/time.h>




using namespace cv;
using namespace std;
//using namespace alglib;

Size blockSize(16,16);
Size blockStride(8,8);
Size cellSize(8,8);
Size winSize(64,128);

int nbins = 9;




void getHogDescriptor(Mat img1, vector<float> & descriptor)
{
                      // Create a hog
                    HOGDescriptor hog(winSize, blockSize, blockStride, cellSize, nbins);

                    // Find descriptor
                  //  vector<float> desc;
                    Mat img = img1.clone();

                    hog.compute(img,descriptor);               // computing HOG feature for an image & storing it in "desc".

}



void SVMtrain(CvSVM &SVM, int no_Of_pos, int no_Of_neg,  vector < vector <float> >  descriptor_table,vector<int> label_table)
{
    // Set up training data
     int no_Of_training = no_Of_pos + no_Of_neg;
     //    Mat labelsMat(no_Of_training, 1, CV_8UC1, label_table);
     Mat labelsMat = Mat(label_table).clone();
    //  cout << labelsMat <<endl ;

    Mat trainingDataMat(descriptor_table.size(), descriptor_table.at(0).size(), CV_32FC1);
    for(int i=0; i<trainingDataMat.rows; ++i)
         for(int j=0; j<trainingDataMat.cols; ++j)
              trainingDataMat.at<float>(i, j) = descriptor_table.at(i).at(j);
   // cout << trainingDataMat << endl;

  //  Mat trainingDataMat = Mat(descriptor_table).clone();

  // Set up SVM's parameters
    CvSVMParams params;
    params.svm_type    = CvSVM::C_SVC;
    params.kernel_type = CvSVM::RBF;
    params.term_crit   = cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS, 1000, 1e-6);

    // Train the SVM
    SVM.train_auto(trainingDataMat, labelsMat, Mat(), Mat(), params);
    cout << SVM.get_support_vector_count() <<endl;
    cout << SVM.get_support_vector(0) <<endl;
    SVM.save("classifier_new.xml");

}


int main()
{
    // SVM Training.
    CvSVM SVM;

    fstream fs("classifier_new.xml");
    if (fs.is_open())
        SVM.load("classifier_new.xml");
    else
    {

        ifstream positiveList("/home/arnab/tcs/dataset/INRIAPerson/train_64x128_H96/pos.lst"), negativeList("/home/arnab/tcs/dataset/INRIAPerson/train_64x128_H96/neg.lst");

        if(!positiveList.is_open() || !negativeList.is_open())
        {
            cerr << "Error opening image names files, exiting..." << endl;
            exit(-1);
        }



        // This vector stores all the desctipors of all the images (imageNum x 1792)
        vector < vector <float> >  descriptor_table;
        vector<int> label_table;

        // starting training of positive images.

      int no_Of_pos = 2416;

      for(int img_count = 0; img_count < no_Of_pos ; img_count++)
        {
            string image ;
            positiveList >> image;

            Mat img1 = imread(image);
            if (img1.empty())
            {
                cout << "Image not read" << endl;
                exit(-1);
            }
       //     cout << "Test4" << endl;

            // Crop image to 64x128 size (from 96x160)
            Rect frame(16,16,64,128);
            Mat img2 = img1(frame).clone();

            vector<float>  descriptor;

            getHogDescriptor(img2,descriptor);
            descriptor_table.push_back(descriptor);
            label_table.push_back(1);

        }

        // end of training of positive images.
        cout<<"End of Training of positive images" <<endl;
        // starting training of negative images.
        int no_Of_neg = 121;
        for(int img_count = 0; img_count < no_Of_neg ; img_count++)
        {
            string image ;
            negativeList >> image;

            Mat img1 = imread(image);

            if (img1.empty())
            {
                cout << "Image not read" << endl ;
                exit(-1);
            }
            int height = img1.rows;
            int width = img1.cols;

            // Crop image to 64x128 size (from 320x240)
            for (int window_counter = 0 ; window_counter <10 ; window_counter++)
            {
                int i = rand() % (width-64);
                int j = rand() % (height-128);
                cout << i <<" , "<<j<<endl;
                Rect frame(i,j,64,128);
                Mat img2 = img1(frame).clone();

                vector<float>  descriptor;
                getHogDescriptor(img2,descriptor);
                descriptor_table.push_back(descriptor);
                label_table.push_back(-1);
            }
            cout << "------------------------------------------------------"<<endl;

         }
        cout<<"End of Training of negative images" <<endl;
         SVMtrain(SVM, no_Of_pos,no_Of_neg,descriptor_table,label_table);
     }

    // Testing.
    int choice = 1;

    if (choice == 1) {

     ifstream positiveList_test("/home/arnab/tcs/dataset/INRIAPerson/Test/pos.lst"), negativeList_test("/home/arnab/tcs/dataset/INRIAPerson/Test/neg.lst");


     if(!positiveList_test.is_open() || !negativeList_test.is_open())
     {
         cerr << "Error opening image names files, exiting..." << endl;
         exit(-1);
     }


     // This vector stores all the desctipors of all the images (imageNum x 1792)
     vector < vector <float> >  descriptor_table_test;
     //  vector<int> label_table;

     // starting testing of positive images.

     int no_Of_pos_test = 288;
     bool res;
     int false_negative = 0, false_positive = 0;



     for(int img_count = 0; img_count < no_Of_pos_test ; img_count++)
     {

         cout << 111 << endl;
         //ros::Time t1 = ros::Time::now();
         clock_t begin = clock();
         string image ;
         positiveList_test >> image;

         Mat img1 = imread(image);
        // imshow("sample",img1);
       //  waitKey(0);
         if (img_count == 40) {
         if (img1.empty())
         {
             cout << "Image not read" << endl;
             exit(-1);
         }
         Mat img_org = img1.clone();

         int height = img1.rows;
         int width = img1.cols;
         float scale = 1.2;
         Mat img2 = img1.clone();

         Vector<Vec3f> Rect_positions;
         float scale_counter = 1;

         while (height >=128 && width >=64)
         {
             //Mat img2 = img1.clone();

             int margin_X = (width % 8)/2;
             int margin_Y = (height % 8)/2;
            // cout << margin_X << " " <<margin_Y <<endl;
            // cout << height << " " << width <<endl;
             int no_Of_Vert_window,no_Of_Hor_Window;
             if ((width-64)%8 == 0)
                 no_Of_Hor_Window = (width-64)/8 +1;
             else
                 no_Of_Hor_Window = (width-64)/8;

             if ((height-128) % 8 == 0)
                 no_Of_Vert_window = (height-128)/8 +1;
             else
                 no_Of_Vert_window = (height-128)/8;
            // cout << no_Of_Hor_Window <<" "<<no_Of_Vert_window<<endl;

             for (int i=0 ; i<no_Of_Vert_window; i++)
                 for (int j=0 ; j<no_Of_Hor_Window; j++)
                 {
                    // cout << i << " " <<j <<endl;
                     Rect frame( (j*8)+margin_X, (i*8)+margin_Y,64,128);
                     Mat img3 = img2(frame).clone();

                     vector<float>  descriptor;
                     getHogDescriptor(img3,descriptor);
                     Mat test = Mat(descriptor).clone();

                     float respo = SVM.predict(test);
                     if(respo == 1)
                     {
                          Vec3f position((j*8)+margin_X , (i*8)+margin_Y, scale_counter);
                          Rect_positions.push_back(position);
                     }

                 }


             height = int(float(height) / scale) ;
             width = int(float(width) / scale) ;
             img1 = img2.clone();
             resize(img1,img2,Size(), 0.83333,0.83333);
             scale_counter ++;
            // cout << scale_counter <<endl ;

          }

         float scale_abs;

         for (int i=0; i<Rect_positions.size(); i++)
         {
             float x_norm = Rect_positions[i][0];
             float y_norm = Rect_positions[i][1];
             if (Rect_positions[i][2] == 1)
                 scale_abs = 1;
             else
                 scale_abs = pow(1.2,(Rect_positions[i][2] - 1));

             Point pt1,pt2;
             pt1.x = x_norm * scale_abs;
             pt1.y = y_norm * scale_abs;
             pt2.x = pt1.x + 64*scale_abs;
             pt2.y = pt1.y + 128*scale_abs;

             int thickness = 1;
             int lineType = 8;
             rectangle(img_org,pt1,pt2,Scalar(0,0,255),thickness,lineType);
         }


         imshow("Image" , img_org);
         waitKey(0);

         //  ros::Time t2 = ros::Time::now();
           clock_t end = clock();
         //  ros::Duration dur = t2-t1;
           double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
           cout << "Time for one image(in sec) : " << elapsed_secs <<endl;

      }
    }
    }

    else {
        ifstream positiveList_test("/home/arnab/tcs/dataset/INRIAPerson/Test/pos.lst"), negativeList_test("/home/arnab/tcs/dataset/INRIAPerson/Test/neg.lst");


        if(!positiveList_test.is_open() || !negativeList_test.is_open())
        {
            cerr << "Error opening image names files, exiting..." << endl;
            exit(-1);
        }


        // This vector stores all the desctipors of all the images (imageNum x 1792)
        vector < vector <float> >  descriptor_table_test;
        //  vector<int> label_table;

        // starting testing of positive images.

        int no_Of_pos_test = 288;
        bool res;
        int false_negative = 0, false_positive = 0;



        for(int img_count = 0; img_count < no_Of_pos_test ; img_count++)
        {

            cout << 111 << endl;
            //ros::Time t1 = ros::Time::now();
            clock_t begin = clock();
            string image ;
            positiveList_test >> image;

            Mat img1 = imread(image);
           // imshow("sample",img1);
          //  waitKey(0);
           if (img_count == 40) {
            if (img1.empty())
            {
                cout << "Image not read" << endl;
                exit(-1);
            }
            Mat img_org = img1.clone();

         //   height, width = img1.shape[:2];

            int height = img1.rows;
            int width = img1.cols;
            float scale = 1.2;
            Mat img2 = img1.clone();


            float scale_counter = 1;

            while (height >=128 && width >=64)
            {
                 //Mat img1,img2;
                 Vector<Vec3f> Rect_positions;
                int margin_X = (width % 8)/2;
                int margin_Y = (height % 8)/2;
               // cout << margin_X << " " <<margin_Y <<endl;
               // cout << height << " " << width <<endl;

                int no_Of_Hor_Window = (width-64)/8 +1;
                int no_Of_Vert_window = (height-128)/8 +1;

               // cout << no_Of_Hor_Window <<" "<<no_Of_Vert_window<<endl;
                Mat img_show = img2.clone();
                for (int i=0 ; i<no_Of_Vert_window; i++)
                {
                    for (int j=0 ; j<no_Of_Hor_Window; j++)
                    {
                       // cout << i << " " <<j <<endl;
                        Rect frame( (j*8)+margin_X, (i*8)+margin_Y,64,128);
                        Mat img3 = img2(frame).clone();

                        vector<float>  descriptor;
                        getHogDescriptor(img3,descriptor);
                        Mat test = Mat(descriptor).clone();

                        float respo = SVM.predict(test);
                        if(respo == 1)
                        {
                             Vec3f position((j*8)+margin_X , (i*8)+margin_Y, scale_counter);
                             Rect_positions.push_back(position);

                          /*   float scale_abs;
                             if (scale_counter == 1)
                                 scale_abs = 1;
                             else
                                 scale_abs = pow(1.2,(scale_counter - 1)); */



                        }

                    }
                }


                height = int(float(height) / scale) ;
                width = int(float(width) / scale) ;
                img1 = img2.clone();
            //    img2.rows = height;
           //     img2.cols = width;
                resize(img1,img2,Size(),0.83333,0.83333);
              //   cout << "Showing scaled image of size " <<img2.cols<<" x "<<img2.rows <<endl;
             //   imshow("Resized" , img2 );
             //   waitKey(0);
                // cout << "test size " << img2.cols <<" x "<<img2.rows <<endl;
                scale_counter ++;
               // cout << scale_counter <<endl ;

                for (int i=0; i<Rect_positions.size(); i++)
                {
                    float x_norm = Rect_positions[i][0];
                    float y_norm = Rect_positions[i][1];

                    Point pt1,pt2;
                    pt1.x = x_norm;
                    pt1.y = y_norm;
                    pt2.x = pt1.x + 64;
                    pt2.y = pt1.y + 128;

                    int thickness = 1;
                    int lineType = 8;
                    rectangle(img_show,pt1,pt2,Scalar(0,0,255),thickness,lineType);
                }
               cout << "Showing scaled image of size " <<img_show.cols<<" x "<<img_show.rows <<endl;

               imshow("Image" , img_show);
               waitKey(0);

             }


              clock_t end = clock();

              double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
              cout << "Time for one image(in sec) : " << elapsed_secs <<endl;

         }
        }
       }







     // end of testing of positive images.


     // starting testing of negative images.
/*     int no_Of_neg_test = 453;
     for(int img_count = 0; img_count < no_Of_neg_test ; img_count++)
     {
         string image ;
         negativeList_test >> image;

         Mat img1 = imread(image);

         if (img1.empty())
         {
             cout << "Image not read2" << endl ;
             exit(-1);
         }

         for (int window_counter =0 ; window_counter <10 ; window_counter++)
         {
             int i = rand() % 145;
             int j = rand() % 55;
             Rect frame(i,j,64,128);

         // Crop image to 64x128 size (from 240x320)

             Mat img2 = img1(frame).clone();

             vector<float>  descriptor;
             getHogDescriptor(img2,descriptor);

             Mat test = Mat(descriptor).clone();

             float respo = SVM.predict(test);
             if(respo == 1)
                  false_positive ++;
          }

       //  descriptor_table_test.push_back(descriptor);
     }
     cout << "False Positive =" << endl << false_positive <<endl;

     int true_positive = no_Of_pos_test - false_negative;
     int true_negative = (no_Of_neg_test*10) - false_positive;

     cout << "True Positive = " << true_positive <<endl;

     cout << "True Negative = " << true_negative <<endl;

     float precision = float (true_positive) /(float(true_positive) + float(false_positive));
     float recall = float (true_positive) / (float(true_positive) + float(false_negative));

     cout << "Precision = " <<  precision << endl;
     cout << "Recall = " << recall <<endl;      */

     // end of testing of negative images.
 /*    Mat testingDataMat(descriptor_table_test.size(), descriptor_table_test.at(0).size(), CV_32FC1);
     for(int i=0; i<testingDataMat.rows; ++i)
          for(int j=0; j<testingDataMat.cols; ++j)
               testingDataMat.at<float>(i, j) = descriptor_table_test.at(i).at(j);

    // SVM.predict(testingDataMat);
   */

}



